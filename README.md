## BashlyBlog

<img src="https://img.shields.io/github/license/mashape/apistatus.svg" alt="license"> <img src="https://img.shields.io/badge/made%20for-bash-blue.svg" alt="made for bash">

- [Dependencies](#dependencies)
    - [Debian](#debian)
    - [macOS](#macos)
- [Install](#install)
- [Configuration](#configuration)
- [Create Post](#create-post)
- [Publish the Blog](#publish-the-blog)

BashlyBlog is a simple and small bash script for creating a blog. It reads all the Markdown files from a folder and creates a blog.

### Dependencies

To use the script, please install the following dependencies

```bash
markdown curl sed
```
#### Debian
```bash
apt install markdown curl sed
```

#### macOS
```bash
brew install markdown curl
```

### Install

To install the script simply clone this repo

```bash
git clone https://codeberg.org/maltekiefer/BashlyBlog
```

Make the script executable

```bash
chmod +x bashlyblog.sh
```

### Configuration

To configure the blog, just rename the file
```bash
config.example
```
to
```bash
config
```
Then adjust the corresponding parameters in the `config` file.

**For each update, the config.example is recreated. Here you can find the corresponding changes to the configuration parameters or new configuration parameters.**

### Create Post

Create posts in the folder `posts`.
Important when creating posts is the filename. The first 12 characters must include the date and time (4 chars!) of creation to be displayed correctly in the blog:

```bash
201907311511-First-Post.md
```

Furthermore, the file must be structured as follows

```markdown
First line is the title

In the third line the content will be started.
```
### Publish the Blog

To publish the blog, simply copy the following files to the server

```bash
- index.html
- feed.rss
```
