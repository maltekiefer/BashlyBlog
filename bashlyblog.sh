#!/bin/bash

##########
##config##
##########

### check if config file exists
### read in config file
if [ -f "config" ]; then
    source config
else
    echo "#########################################################\n"
    echo "No config file. Please create a config file.\n"
    echo "#########################################################\n"
    exit 1
fi

######################
## DON'T CHANGE HERE##
######################

version="0.0.7"
versionurl="https://codeberg.org/maltekiefer/BashlyBlog/raw/branch/master/VERSION"
bashlyurl="https://codeberg.org/maltekiefer/BashlyBlog/raw/branch/master/bashlyblog.sh"
templateurl="https://codeberg.org/maltekiefer/BashlyBlog/raw/branch/master/template.html"
configurl="https://codeberg.org/maltekiefer/BashlyBlog/raw/branch/master/config.example"
changelogurl="https://codeberg.org/maltekiefer/BashlyBlog/raw/branch/master/CHANGELOG.md"
BASH=$(whereis bash)
CURL=$(whereis curl)
GET_PLATFORM="$(uname)"

##########
##update##
##########

function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

update() {

    serverversion=$($CURL --silent "$versionurl")


    if [ $(version $serverversion) -ge $(version $version) ]; then
        echo "#########################################################"
        echo "There is a new version. Do you want to see the Changelog?"
        echo "#########################################################"
        read -p "Yes(y)/No(n) " -r changelog

        if [[ $changelog =~ ^[Yy]$ ]]
        then
            $CURL --silent "$changelogurl"
        fi

        echo "#########################################################"
        echo "Do you want to update?"
        echo "#########################################################"
        read -p "Yes(y)/No(n) " -r update

        if [[ $update =~ ^[Yy]$ ]]
        then
            echo "#########################################################"
            echo "I am updating myself..."
            echo "#########################################################"
            sleep 4

            $CURL --silent "$bashlyurl" > bashlyblog.sh
            $CURL --silent "$configurl" > config.example

            if [ $allow_template_update -eq "1" ];
            then
                echo "#########################################################"
                echo "Template file will be updated......"
                echo "#########################################################"
                $CURL --silent "$templateurl" > template.html
            fi

            echo "#########################################################"
            echo "Update done. Restart..."
            echo "#########################################################"
            $BASH bashlyblog.sh
            exit 1
        fi
    fi
}

##########
## rss ##
##########
create_rss() {

    date_today=$(LC_ALL=C date +"%a, %d %b %Y %H:%M:%S %z")

    echo '<?xml version="1.0" encoding="UTF-8" ?>'
    echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">'
    echo "<channel><title>${title}</title><link>${url}/index.html</link>"
    echo "<description>${desc}</description><language>en</language>"
    echo "<lastBuildDate>${date_today}</lastBuildDate>"
    echo "<pubDate>${date_today}</pubDate>"
    echo "<atom:link href=\"${url}/feed.rss\" rel=\"self\" type=\"application/rss+xml\" />"


    for i in `ls -rt posts/*.md`;
    do
        title_post=$(cat $i | head -n1 | markdown | sed "s/<p>//g" | sed "s/<\/p>//g")
        date_post=$(echo $i | cut -c7-18 )
        echo $date_post
        if [ "$GET_PLATFORM" == 'Linux' ]; then
          date_post=$(LC_ALL=C date -d "$date_post"  +"%a, %d %b %Y %H:%M:%S %z")
        elif [ "$GET_PLATFORM" == 'FreeBSD' ] || [ "$GET_PLATFORM" == 'Darwin' ]; then
          date_post=$(LC_ALL=C date -jf "%Y%m%d%H%M" "$date_post" +"%a, %d %b %Y %H:%M:%S %z")
        fi
        content_post=$(cat $i | sed '1,1d' | sed '$d' | markdown)
        echo '<item><title>'
        echo $title_post
        echo '</title><description><![CDATA['
        echo $content_post
        echo "]]></description><link>${url}/index.html</link>"
        echo "<guid>${url}/index.html</guid>"
        echo "<dc:creator>${author}</dc:creator>"
        echo "<pubDate>${date_post}</pubDate></item>"
    done

    echo '</channel></rss>'
}
##########
## base ##
##########

if [ $allow_auto_update -eq "1" ];
then
    update
fi

protected_mail=${author_mail//@/&#64;}
protected_mail=${protected_mail//./&#46;}

if [ -d "tmp/" ]; then
    rm -rf tmp/
    mkdir tmp/
else
    mkdir tmp/
fi

if [ -f "index.html" ]; then
    rm index.html
fi
if [ -f "entries.html" ]; then
    rm entries.html
fi
if [ -f "feed.rss" ]; then
    rm feed.rss
fi

for i in `ls -rt posts/*.md`;
do
  markdown $i > tmp/`basename $i .md`.html;
done

ls -rt tmp/* | xargs -n 1 awk 'BEGIN {print "<div class=\"entry\">" } {print $0} END {print "</div>"}' > entries.html
sed -e '/<!-- insert here/r entries.html' template.html > index.html
sed -i'.original' -e "s/{{author}}/${author}/g" index.html
sed -i'.original' -e "s/{{title}}/${title}/g" index.html
sed -i'.original' -e "s/{{desc}}/${desc}/g" index.html
rm *.original

create_rss 3>&1 > feed.rss
echo "Blog has been created"
echo "Please copy the following files to your server:"
echo "- index.html"
echo "- feed.rss"
