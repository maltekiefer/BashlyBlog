## Changelog

### [0.0.7] - 2020-05-04
#### Changed
- Updated README to latest version

### [0.0.6] - 2020-05-04
#### Added
- check path of `bash` and `curl`
- support FreeBSD and macOS date format

#### Changed
- Created a posts folder for better overview
- check if file really exists before try to delete file
- added minutes and seconds and filename
- update function is perfomanter now

#### Removed
- color prompt


### [0.0.5] - 2020-05-04
#### Fixed
- sed error on macOS

### [0.0.4] - 2019-07-31
#### Added
- Ask if Changelog should be displayed
- Ask if script should be updated
- Check if config file exists, if not abort script.
- config.example file
- config: new var to allow / disallow auto update on start
- config: new var to allow / disallow update of template file
- template: CSS for codeblock

#### Changed
- Update function update config.example file
- Update function checks if allow_template_update is set

#### Fixed
- check if config file exists

### [0.0.3] - 2019-07-31
#### Added
- Add possiblity to use a config file
- Add template file
- Add update for template file

#### Fixed
- text color shown correctly now

### [0.0.2] - 2019-07-30
#### Added
- Self update function

### [0.0.1] - 2019-07-30
#### Added
- first version
